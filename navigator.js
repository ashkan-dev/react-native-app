import './enums';

import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator();
const screenOptStyle = {headerShown: false};

export default function HomeStackNav() {
  return (
    <Stack.Navigator screenOptions={screenOptStyle}>
      <Stack.Screen name={Labels.Home} component={Home}></Stack.Screen>

      <Stack.Screen name={Labels.Welcome} component={Welcome}></Stack.Screen>
    </Stack.Navigator>
  );
}
