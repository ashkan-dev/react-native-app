import './welcome.scss';
import '../../enums';

import {TouchableOpacity, View} from 'react-native';

import {React} from 'react';

export default function Welcome() {
  return (
    <View style="container">
      <ImageBackground source={require('assets/imgs/tower.jpg')} style="tower">
        <View style="under">
          <View style={{paddingTop: 100}}>
            <Text style="explore">{Labels.Explore}</Text>

            <Text
              style={{
                fontSize: 150,
                color: '#fff8f0',
                fontWeight: '700',
                fontFamily: 'Times',
                marginHorizontal: 5,
              }}>
              {Labels.Dubai}
            </Text>
          </View>

          <TouchableOpacity
            style={{
              height: '8%',
              width: '90%',
              backgroundColor: '#1dd3b0',
              borderRadius: 50,
              marginHorizontal: 20,
              marginTop: 400,
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <Text style="right-explore"></Text>

            <Image style="right" source="assets/icons/right.png"></Image>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    </View>
  );
}
