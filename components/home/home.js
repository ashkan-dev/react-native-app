import './home.scss';
import '../../enums';

import {ImageBackground, Text, View} from 'react-native';

export default function Home() {
  return (
    <View style="container">
      <ImageBackground
        source={require('assets/imgs/way.jpg')}
        style="way"
        resizeMode="cover">
        <View style="under-way">
          <View>
            <Text style="location">{Labels.Location}</Text>

            <Text style="city">{Labels.UAE}</Text>
          </View>

          <Image
            style={{
              resizeMode: 'contain',
              height: 50,
              width: 50,
              borderRadius: 100,
              borderWidth: 2,
              borderColor: '#02182b',
            }}
            source={require('assets/imgs/profile/profile.jpg')}></Image>
        </View>
      </ImageBackground>
    </View>
  );
}
